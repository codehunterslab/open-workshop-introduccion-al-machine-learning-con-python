#!sudo apt install tesseract-ocr
#!pip install Pillow
#!pip install pytesseract

from PIL import Image
import pytesseract
def ocr_core(filename):
  text = pytesseract.image_to_string(Image.open(filename))
  return text

print(ocr_core('path'))